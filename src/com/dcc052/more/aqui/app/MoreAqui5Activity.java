package com.dcc052.more.aqui.app;

import static com.dcc052.more.aqui.app.entity.EntitiesConstants.FROM;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.TABLE_NAME;
import server.Command;
import server.DAO;
import server.DaoImpl;
import server.Invoker;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.dcc052.more.aqui.app.entity.EstateLocation;
import com.dcc052.more.aqui.app.entity.EstatesData;

/**
 * This class is responsible for the behavior of application main screen.
 * 
 * @author joao.montandon
 * 
 */
public class MoreAqui5Activity extends Activity implements OnClickListener,
		Command {

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	public final void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_more_aqui5);

		// retrieve the buttons and define their event
		Button newBtn = (Button) findViewById(R.id.btn_new);
		newBtn.setOnClickListener(this);
		Button showBtn = (Button) findViewById(R.id.btn_search);
		showBtn.setOnClickListener(this);
		Button mapsBtn = (Button) findViewById(R.id.btn_maps);
		mapsBtn.setOnClickListener(this);
		Button saveBtn = (Button) findViewById(R.id.btn_save);
		saveBtn.setOnClickListener(this);
	}

	/**
	 * Method that will handle the onclick events on the screen.
	 * 
	 * @param v
	 *            the view that triggered the event
	 */
	public final void onClick(final View v) {

		// execute the action according to the view id
		Intent intent;
		switch (v.getId()) {

		case R.id.btn_new:
			// call InsertActivity
			intent = new Intent(this, InsertActivity.class);
			startActivity(intent);
			break;

		case R.id.btn_search:
			// call ShowActivity
			intent = new Intent(this, ShowActivity.class);
			startActivity(intent);
			break;

		case R.id.btn_maps:
			// Call MapActivity
			intent = new Intent(this, ShowAddressActivity.class);
			startActivity(intent);
			break;

		case R.id.btn_save:
			// save the estates into the server
			Log.v(this.getClass().getName(), "Saving the Estates");

			Invoker invoker = new Invoker(DAO.HOST, DAO.PORT);
			invoker.invoke(new DaoImpl(), this);
			break;

		default:
			// unhandled case
			Log.w(this.getClass().getName(),
					"Unhandled control at onClick event: " + v.getId());
			break;

		}
	}


	/**
	 * Method that executes the command to save the estates into the remote
	 * server.
	 * 
	 * @param d
	 *            the DAO implementation that will save the estates
	 */
	public final void execute(final DaoImpl d) {
		// initialize the database
		EstatesData estatesDb = new EstatesData(this);
		SQLiteDatabase db = estatesDb.getReadableDatabase();
		Cursor cursor = db
				.query(TABLE_NAME, FROM, null, null, null, null, null);
		startManagingCursor(cursor);

		//iterate over the cursor and transfer the estates to the remote server
		while (cursor.moveToNext()) {
			EstateLocation estate = new EstateLocation(
					cursor.getString(4), cursor.getString(2), cursor.getInt(1),
					cursor.getString(3), cursor.getDouble(5),
					cursor.getDouble(6));

			Log.v(this.getClass().getName(), "Saving Estate " +  cursor.getLong(0));
			d.add(cursor.getLong(0), estate);

		}
	}
}
