package com.dcc052.more.aqui.app;

import static com.dcc052.more.aqui.app.entity.EntitiesConstants.PHONE;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.SIZE;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.STATUS;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.TABLE_NAME;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.TYPE;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.LAT;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.LONG;
import android.app.Activity;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.dcc052.more.aqui.app.entity.EstatesData;

/**
 * Class that handle the Insert screen behavior.
 * 
 * @author joao.montandon
 */
public class InsertActivity extends Activity implements OnClickListener,
		LocationListener {

	/**
	 * The minimum time between the location manager updates.
	 */
	private static final int MIN_TIME = 15000;

	/**
	 * Activity's location manager that will be used to capture the current
	 * position of the user and insert it into the database.
	 */
	private LocationManager locationManager;

	/**
	 * Confirmation button of the insert screen.
	 */
	private Button okbtn;

	/**
	 * Application database object.
	 */
	private EstatesData db;

	/**
	 * Object that stores the sensor that will be used to locate the coordinates
	 * of the phone.
	 */
	private String bestProvider;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	public final void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_insert);

		// define the button event
		okbtn = (Button) findViewById(R.id.btn_ins_ok);
		okbtn.setOnClickListener(this);

		// initialize the database
		db = new EstatesData(this);

		// initialize the locationManager
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		bestProvider = locationManager.getBestProvider(new Criteria(), true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onPause()
	 */
	@Override
	protected final void onPause() {
		super.onPause();
		locationManager.removeUpdates(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected final void onResume() {
		super.onResume();
		locationManager.requestLocationUpdates(bestProvider, MIN_TIME, 1, this);
	}

	/**
	 * Method that will handle the onclick events on the screen.
	 * 
	 * @param v
	 *            the view that triggered the event
	 */
	public final void onClick(final View v) {

		// load the component interfaces
		EditText text = (EditText) findViewById(R.id.txt_ins_phone);
		CheckBox checkBox = (CheckBox) findViewById(R.id.chk_ins_construction);
		RadioGroup typeRadio = (RadioGroup) findViewById(R.id.rdg_ins_type);
		RadioGroup sizeRadio = (RadioGroup) findViewById(R.id.rdg_ins_size);

		// check if the text inserted into phone field is valid
		boolean podeCadastrar = true;
		for (char c : text.getText().toString().toCharArray()) {
			if (c < '0' || c > '9') {
				podeCadastrar = false;
			}
		}

		// insert it if everything is correct
		if (text.getText().toString().length() > 0 && podeCadastrar) {
			Integer phone = Integer.parseInt(text.getText().toString());
			String type = ((RadioButton) findViewById(typeRadio
					.getCheckedRadioButtonId())).getText().toString();
			String size = ((RadioButton) findViewById(sizeRadio
					.getCheckedRadioButtonId())).getText().toString();
			String inConstruction = checkBox.isChecked() + "";

			// retrieve the phone's current location
			Location location = locationManager
					.getLastKnownLocation(bestProvider);

			double latNow = 0;
			double longNow = 0;
			if (location != null) {
				latNow = location.getLatitude();
				longNow = location.getLongitude();
			}

			Log.v(this.getClass().getName(), "Lat: " + latNow + ", Long: "
					+ longNow);

			// insert the object into the database
			SQLiteDatabase sqlDb = db.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put(PHONE, phone);
			values.put(TYPE, type);
			values.put(SIZE, size);
			values.put(STATUS, inConstruction);
			values.put(LONG, longNow);
			values.put(LAT, latNow);
			sqlDb.insertOrThrow(TABLE_NAME, null, values);
			sqlDb.close();

			// message to inform that the object was persisted
			Log.v(this.getClass().getName(), "Object inserted successfully.");
			Toast.makeText(this, getString(R.string.ins_ok_msg),
					Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(this, getString(R.string.ins_error_msg),
					Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * Method which is executed when the location is changed.
	 * 
	 * @param arg0
	 *            the new location coordinates
	 */
	public final void onLocationChanged(final Location arg0) {
		Log.v(this.getClass().toString(), "Location changed: " + arg0);
	}

	/**
	 * Method which is executed when a provider is disabled.
	 * 
	 * @param arg0
	 *            The provided that has been disabled
	 */
	public final void onProviderDisabled(final String arg0) {
		Log.v(this.getClass().getName(), "Provider Disabled: " + arg0);
	}

	/**
	 * Method which is executed when a provider is enabled.
	 * 
	 * @param arg0
	 *            the enabled provider
	 */
	public final void onProviderEnabled(final String arg0) {
		Log.v(this.getClass().getName(), "Provider Enabled: " + arg0);
	}

	/**
	 * Method which is executed when a provider changes its status.
	 * 
	 * @param provider
	 *            the provider that has been changed
	 * @param status
	 *            the status that has changed
	 * @param extras
	 *            extra information
	 */
	public void onStatusChanged(final String provider, final int status,
			final Bundle extras) {

	}

}
