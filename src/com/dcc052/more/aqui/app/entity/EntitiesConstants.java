package com.dcc052.more.aqui.app.entity;

import static android.provider.BaseColumns._ID;

/**
 * Database Constants.
 * 
 * @author joao.montandon
 * 
 */
public interface EntitiesConstants {

	/**
	 * name of the table where the estates will be persisted.
	 */
	String TABLE_NAME = "estates";
	/**
	 * name of the column type.
	 */
	String TYPE = "type";
	/**
	 * name of the column status.
	 */
	String STATUS = "Status";
	/**
	 * name of the column size.
	 */
	String SIZE = "Size";
	/**
	 * name of the column phone.
	 */
	String PHONE = "Phone";
	/**
	 * name of the column latitude.
	 */
	String LAT = "latitude";
	/**
	 * name of the column longitude.
	 */
	String LONG = "longitude";
	/**
	 * Table's columns.
	 */
	String[] FROM = new String[] { _ID, PHONE, SIZE,
			STATUS, TYPE, LAT, LONG };
}