package com.dcc052.more.aqui.app.entity;

import static android.provider.BaseColumns._ID;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.PHONE;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.SIZE;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.STATUS;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.TABLE_NAME;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.TYPE;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.LAT;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.LONG;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * @author joao.montandon
 * 
 */
public class EstatesData extends SQLiteOpenHelper {

	/**
	 * database's name.
	 */
	private static final String DB_NAME = "moreaqui.db";

	/**
	 * database's version.
	 */
	private static final int DB_VERSION = 1;

	/**
	 * Database's constructor.
	 * 
	 * @param ctx
	 *            context that intialize the database
	 */
	public EstatesData(final Context ctx) {
		super(ctx, DB_NAME, null, DB_VERSION);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite
	 * .SQLiteDatabase)
	 */
	@Override
	public final void onCreate(final SQLiteDatabase db) {

		String query = "CREATE TABLE " + TABLE_NAME + "(" + _ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + TYPE
				+ " TEXT NOT NULL, " + SIZE + " TEXT NOT NULL, " + STATUS
				+ " TEXT, " + PHONE + " TEXT NOT NULL, " + LAT + " DOUBLE, "
				+ LONG + " DOUBLE);";
		db.execSQL(query);

		Log.v(this.getClass().getName(), "Database created: " + query);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.database.sqlite.SQLiteOpenHelper
	 * #onUpgrade(android.database.sqlite
	 * .SQLiteDatabase, int, int)
	 */
	@Override
	public final void onUpgrade(final SQLiteDatabase db, final int oldVersion,
			final int newVersion) {

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		Log.v(this.getClass().getName(), "Upgrading database");
		onCreate(db);
	}

}