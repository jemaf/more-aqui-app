package com.dcc052.more.aqui.app.entity;

import java.io.Serializable;

/**
 * Extended class that includes the Estate's location.
 * 
 * @author edumontandon
 * 
 */
public class EstateLocation extends Estate implements Serializable {

	/** The serial version of this class. */
	private static final long serialVersionUID = -1796217464041880091L;

	/**
	 * Estate's latitude.
	 */
	private double latitude;

	/**
	 * Estate's Longitude.
	 */
	private double longitude;

	/**
	 * Default constructor.
	 */
	public EstateLocation() {
		super();
	}
	
	/**
	 * Inherited constructor.
	 * 
	 * @param type
	 *            Estate's type
	 * @param size
	 *            Estate's size
	 * @param phone
	 *            Estate's phone
	 * @param inConstruction
	 *            String that indicates if the Estate is under construction
	 */
	public EstateLocation(final String type, final String size,
			final int phone, final String inConstruction) {
		super(type, size, phone, inConstruction);
	}

	/**
	 * Inherited constructor.
	 * 
	 * @param type
	 *            Estate's type
	 * @param size
	 *            Estate's size
	 * @param phone
	 *            Estate's phone
	 * @param inConstruction
	 *            String that indicates if the Estate is under construction
	 * @param latitudep
	 *            Estate's latitude
	 * @param longitudep
	 *            Estate's longitude
	 */
	public EstateLocation(final String type, final String size,
			final int phone, final String inConstruction, final double latitudep,
			final double longitudep) {
		super(type, size, phone, inConstruction);
		this.latitude = latitudep;
		this.longitude = longitudep;
	}

	@Override
	public final String toString() {
		return super.toString() + ", Latitude: " + this.latitude
				+ ", Longitude: " + this.longitude;
	}

}
