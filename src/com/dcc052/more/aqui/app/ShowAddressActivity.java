package com.dcc052.more.aqui.app;

import static android.provider.BaseColumns._ID;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.LAT;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.LONG;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.PHONE;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.SIZE;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.STATUS;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.TABLE_NAME;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.TYPE;

import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;

import com.dcc052.more.aqui.app.entity.EstatesData;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

/**
 * Class that handles the events from the MapActivity of the application.
 * 
 * @author edumontandon
 * 
 */
public class ShowAddressActivity extends MapActivity {

	/**
	 * Variable that handles the mapview object of the screen.
	 */
	private MapView map;

	/**
	 * Table's columns.
	 */
	private static final String[] FROM = new String[] { _ID, PHONE, SIZE,
			STATUS, TYPE, LAT, LONG };

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.android.maps.MapActivity#onCreate(android.os.Bundle)
	 */
	@Override
	public final void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_address);

		// set the zoom control of the screen
		map = (MapView) findViewById(R.id.mapview);
		map.setBuiltInZoomControls(true);

		// iterate over the registers to put the mark into their location
		Cursor cursor = getAllRegisters();

		ArrayList<GeoPoint> gps = new ArrayList<GeoPoint>();
		GeoPoint firstGp = null;

		while (cursor.moveToNext()) {
			double cLat = cursor.getDouble(5);
			double cLong = cursor.getDouble(6);

			GeoPoint currentGp = new GeoPoint((int) (cLat * 1E6),
					(int) (cLong * 1E6));

			gps.add(currentGp);

			Log.v(this.getClass().getName(), currentGp.getLatitudeE6() + " "
					+ currentGp.getLongitudeE6());

			// select the first geopoint
			if (firstGp == null) {
				firstGp = currentGp;
			}

			// create the pins overlay and add it into the overlays list
			PinsOverlay pinsOverlay = new PinsOverlay(gps);
			List<Overlay> overlays = map.getOverlays();
			overlays.clear();
			overlays.add(pinsOverlay);

			// animate the map to the first geopoint
			MapController mapController = map.getController();
			mapController.animateTo(firstGp);
			mapController.setZoom(17);
			map.invalidate();
		}
	}

	/**
	 * Select all the estates registers that exists in the database.
	 * 
	 * @return a cursor with all estates that exists in the database
	 */
	private Cursor getAllRegisters() {
		// intialize the database
		EstatesData estatesDb = new EstatesData(this);
		SQLiteDatabase db = estatesDb.getReadableDatabase();
		Cursor cursor = db.query(TABLE_NAME, FROM, null, null, null, null, _ID);
		startManagingCursor(cursor);

		return cursor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public final boolean onCreateOptionsMenu(final Menu menu) {
		getMenuInflater().inflate(R.menu.activity_show_address, menu);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.android.maps.MapActivity#isRouteDisplayed()
	 */
	@Override
	protected final boolean isRouteDisplayed() {
		return false;
	}

	/**
	 * private class that will handle the icon overlay of the screen.
	 * 
	 * @author edumontandon
	 * 
	 */
	class PinsOverlay extends Overlay {

		/**
		 * geopoint where the figure will be drawed.
		 */
		private List<GeoPoint> gps;

		/**
		 * Constructor class.
		 * 
		 * @param geopoints
		 *            the list of geopoints which the pins will be drawed
		 */
		public PinsOverlay(final List<GeoPoint> geopoints) {
			this.gps = geopoints;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.google.android.maps.Overlay#draw(android.graphics.Canvas,
		 * com.google.android.maps.MapView, boolean, long)
		 */
		@Override
		public boolean draw(final Canvas canvas, final MapView mapView,
				final boolean shadow, final long when) {
			super.draw(canvas, mapView, shadow, when);

			Log.v(this.getClass().getName(), "Drawing the pin overlay");

			// draw the pins
			for (GeoPoint gp : gps) {
				Point screenPoint = new Point();
				map.getProjection().toPixels(gp, screenPoint);
				Bitmap bmp = BitmapFactory.decodeResource(getResources(),
						R.drawable.e1);
				canvas.drawBitmap(bmp, screenPoint.x, screenPoint.y, null);

				Log.v(this.getClass().getName(), "Drawing pin at: "
						+ screenPoint.x + "," + screenPoint.y);
			}
			return true;
		}
	}
}
