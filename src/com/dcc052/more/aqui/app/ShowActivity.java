package com.dcc052.more.aqui.app;

import static com.dcc052.more.aqui.app.entity.EntitiesConstants.PHONE;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.SIZE;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.STATUS;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.TABLE_NAME;
import static com.dcc052.more.aqui.app.entity.EntitiesConstants.TYPE;
import static android.provider.BaseColumns._ID;
import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.SimpleCursorAdapter;

import com.dcc052.more.aqui.app.entity.EstatesData;

/**
 * Class responsible for the behavior of ShowActivity screen.
 * 
 * @author edumontandon
 * 
 */
public class ShowActivity extends ListActivity {

	/**
	 * List of the columns which will have their data retrieved.
	 */
	private static final String[] FROM = new String[] { PHONE, SIZE,
			STATUS, TYPE, _ID };

	/**
	 * List of the components that will bind with the data retrieved.
	 */
	private static final int[] TO = { R.id.phone, R.id.size, R.id.status,
			R.id.type };

	/**
	 * application database.
	 */
	private EstatesData estateDb;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected final void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show);

		estateDb = new EstatesData(this);

		// retrieve the estates of the database
		retrieveEstates();
	}

	/**
	 * Retrieve all the estates of the database.
	 */
	private void retrieveEstates() {
		// Perform a managed query. The Activity will handle closing
		// and re-querying the cursor when needed.
		SQLiteDatabase db = estateDb.getReadableDatabase();
		Cursor cursor = db
				.query(TABLE_NAME, FROM, null, null, null, null, null);
		startManagingCursor(cursor);

		// Set up data binding
		SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
				R.layout.activity_show_row, cursor, FROM, TO);
		setListAdapter(adapter);
		
		db.close();
	}

}
